//
//  BZZRest.m
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import "BZZRest.h"


@implementation BZZRest

+(instancetype) sharedSingleton{
    
    static dispatch_once_t onceToken;
    static BZZRest *shared;
    dispatch_once(&onceToken, ^{
        shared = [[BZZRest alloc] init];
        
    });
    
    return shared;
}

-(AFHTTPSessionManager *)queryAPI{
    
    NSURL *url = [NSURL URLWithString:@"http://wispy-wave-1292.getsandbox.com"];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Auth" : @"Bearer 63bea7d5e84b6c45a4af9f9d3db714a8",
                                                   @"Content-Type" : @"application/json"
                                                   };
    
    
    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url sessionConfiguration:sessionConfiguration];
    
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    sessionManager.responseSerializer = response;
    sessionManager.securityPolicy.allowInvalidCertificates = YES;
    sessionManager.securityPolicy.validatesDomainName = NO;
    //sessionManager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
    
    return sessionManager;
    
}

+(void)sendInternetDownNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"internetDown" object:nil];

}

+(void)sendInternetUpNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"internetUp" object:nil];

}

@end
