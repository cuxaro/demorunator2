//
//  main.m
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
