#import "BZZComment.h"
#import "BZZRunner.h"

@interface BZZComment ()

// Private interface goes here.

@end

@implementation BZZComment

+(instancetype)commentWithDyctionary:(NSDictionary *)dicc withContext:(NSManagedObjectContext *)context{
    
    BZZComment *comment;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[BZZComment entityName]];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identificador == %@", [dicc objectForKey:@"id"]];
    NSError *error;
    NSMutableArray *response = [[context executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    if ([response count] > 0) {
        comment = (BZZComment *)[response objectAtIndex:0];
        
    }else{
        comment = [NSEntityDescription insertNewObjectForEntityForName:[BZZComment entityName]
                                               inManagedObjectContext:context];
        
        comment.runnercomment = [BZZRunner runnerWithDyctionary:[dicc objectForKey:@"user"] withContext:context];
        
        if ([dicc objectForKey:@"comment"] != [NSNull null]) {
            comment.comentario = [dicc objectForKey:@"comment"];
        }
        
        if ([dicc objectForKey:@"id"] != [NSNull null]) {
            comment.identificador = [dicc objectForKey:@"id"];
        }
        


    }
    
    return comment;
}

@end
