//
//  ViewController.m
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import "RunfeedViewController.h"
#import "BZZRest.h"


#import "BZZSimpleCoreDataStack.h"
#import "BZZRun.h"
#import "BZZRunner.h"
#import "BZZComment.h"

#import "BZZFunctionsRunator.h"
#import "BZZMaps.h"

#import "RunfeedCell.h"
@import CoreLocation;
@import MapKit;

@interface RunfeedViewController ()

@property (nonatomic, strong) NSTimer *checkTimelineNewRuns;
@property (nonatomic, strong) BZZSimpleCoreDataStack *model;

@property (nonatomic, strong) NSArray *runs;

@end

@implementation RunfeedViewController


#pragma mark - View LifeCycle

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.model = [BZZSimpleCoreDataStack coreDataStackWithModelName:@"Model"];
    [self designInterface];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    // Descomentar Linea para testear
    //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstStart"];

    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstStart"]) {
        
        [self getTimelineRunsFromAPI];
        [[NSUserDefaults standardUserDefaults] setBool:YES
                                                forKey:@"firstStart"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
    }else{
        
        [self loadRunsFromCoreData];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeReloadButton:)
                                                 name:@"newRuns"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetStateNotification:)
                                                 name:@"internetDown"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetStateNotification:)
                                                 name:@"internetUp"
                                               object:nil];
    
    UINib *cellNib = [UINib nibWithNibName:@"RunfeedCell"
                                    bundle:nil];
    
    [self.tableView registerNib:cellNib
         forCellReuseIdentifier:[RunfeedCell cellID] ];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.checkTimelineNewRuns invalidate];
    self.checkTimelineNewRuns = nil;

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.checkTimelineNewRuns = [NSTimer scheduledTimerWithTimeInterval:40.0
                                                         target:self
                                                       selector:@selector(getTimelineAnyNewRunsFromAPI)
                                                       userInfo:nil
                                                        repeats:true];
        
}


#pragma mark - Design

#pragma mark Interface
-(void)designInterface{
    [self.showNewRuns setHidden:YES];
    self.internetDown.hidden = YES;

    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor grayColor]];
}

#pragma mark Map
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        renderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        renderer.lineWidth   = 3;
        return renderer;
    }
    return nil;
}




#pragma mark - Download Process
-(void)getTimelineRunsFromAPI{
    
   [[[BZZRest sharedSingleton] queryAPI] GET:@"timeline" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            for (NSDictionary *card in [[responseObject objectForKey:@"data"] objectForKey:@"cards"]) {
                [BZZRun newRunWithDyctionary:card withContext:self.model.context];
            }
            
            [BZZRest sendInternetUpNotification];
            [self loadRunsFromCoreData];

        });
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelectorOnMainThread:@selector(getTimelineRunsFromAPI)
                                   withObject:nil
                                waitUntilDone:5.0];
        });
        
        [self requestFailed:error sessionDataTask:task];

    }];
}
-(void)getTimelineAnyNewRunsFromAPI{
    
    [[[BZZRest sharedSingleton] queryAPI] GET:@"timeline/anyNewRun" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            for (NSDictionary *card in [[responseObject objectForKey:@"data"] objectForKey:@"cards"]) {
                [BZZRun newRunWithDyctionary:card withContext:self.model.context];
            }
            
            [BZZRest sendInternetUpNotification];
        });
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self requestFailed:error sessionDataTask:task];
    }];
    
}

-(void)requestFailed:(NSError *)errorMessage sessionDataTask:(NSURLSessionDataTask *)task{
    
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
    int statusCode = (int)response.statusCode;
    if (statusCode == 0){
        
        [BZZRest sendInternetDownNotification];
    }
}


#pragma mark - Notificacions
-(void)activeReloadButton:(NSNotification *)notification{
    
    if ([notification.name isEqualToString:@"newRuns"]) {
        [self.showNewRuns setHidden:NO];
    }
}

-(void)internetStateNotification:(NSNotification *)notification{
    
    if ([notification.name isEqualToString:@"internetDown"]) {
        self.internetDown.hidden = NO;
        
    }else if ([notification.name isEqualToString:@"internetUp"]){
        self.internetDown.hidden = YES;
    }

}

-(void)loadRunsFromCoreData{
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[BZZRun entityName]];
    
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"fecha"
                                                                   ascending:NO]];
    fetchRequest.fetchLimit = 50;
    
    NSError *errorFetch;
    NSArray *response = [self.model.context executeFetchRequest:fetchRequest error:&errorFetch];
    
    if (errorFetch) {
        NSLog(@"El error al realizar la consulta es %@", errorFetch);
    }
    
    if (response) {
        NSLog(@"la respuesta es correcta y tiene %lu runs", (unsigned long)[response count]);
        self.runs = response;
        [self updateRunFeed];
    }
    
    NSError *errorSave;
    [self.model.context save:&errorSave];
    
}

#pragma mark - Table DataSource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    BZZRun *run = [self.runs objectAtIndex:indexPath.row];
    
    RunfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:[RunfeedCell cellID]];
    
    
    //Run Info
    NSDateFormatter *dateFormat = [BZZFunctionsRunator formatDateWithString:@"YYY-MM-dd"];
    cell.runDate.text = [dateFormat stringFromDate:run.fecha];

    NSDateFormatter *timeFormat = [BZZFunctionsRunator formatDateWithString:@"HH:mm"];
    cell.runTime.text = [timeFormat stringFromDate:run.fecha];

    cell.runPlace.text = run.lugar;
    
    cell.runPaceText.text = run.ritmo.description;
    cell.runDistanceText.text = [NSString stringWithFormat:@"%.2f km",run.distancia.floatValue/1000];
    cell.runDurationText.text = [BZZFunctionsRunator convertStandartDurationFormatFromSecond:[run.duracion integerValue]];
    
    //cell.runMapMapa.centerCoordinate = CLLocationCoordinate2DMake(38.535210, -0.128270);
    cell.runMapMapa.centerCoordinate = [BZZMaps decodePolylineWithString:run.mapa];
    cell.runMapMapa.region = MKCoordinateRegionMakeWithDistance([BZZMaps decodePolylineWithString:run.mapa], 4000, 4000);
    cell.runMapMapa.delegate = self;
    [cell.runMapMapa addOverlay:[BZZMaps generatePolylineWith:run.mapa]];

    
    cell.runPhoto.image = [UIImage imageWithData:run.foto];
    
    //Runner Info
    cell.runnerName.text = run.runner.nombre;
    cell.runnerAvatar.image = [UIImage imageWithData:run.runner.foto];

    //Social Info
    cell.runLikes.text = [NSString stringWithFormat:@"%@ likes", run.likes];
    
    NSString *commentWord = [run.comentarios  isEqual: @1] ? @"comentario" : @"comentarios";
    cell.runTotalComments.text = [NSString stringWithFormat:@"%@ %@",run.comentarios, commentWord];
    
    cell.runLastCommentText.text = run.comment.comentario;
    cell.runLastCommentName.text = run.comment.runnercomment.nombre;
    cell.runLastCommentAvatar.image = [UIImage imageWithData:run.comment.runnercomment.foto];

    return cell;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [cell removeFromSuperview];
    cell = nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSLog(@"%lu", (unsigned long)[self.runs count]);
    return [self.runs count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [RunfeedCell height];
}


#pragma mark - RunFeed process

- (IBAction)showNewRunTouch:(id)sender {
    
    [self loadRunsFromCoreData];
    [self updateRunFeed];
}

-(void)updateRunFeed{
    [self.tableView reloadData];
    [self.showNewRuns setHidden:YES];
    [self.tableView setContentOffset:CGPointZero animated:YES];

}
@end
