//
//  RunfeedCell.m
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 29/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import "RunfeedCell.h"
#import "UIView+PREBorderView.h"


@import CoreGraphics;

@implementation RunfeedCell

- (void)awakeFromNib {
    

    self.selectionStyle = false;

    self.runnerAvatar.layer.cornerRadius = self.runnerAvatar.frame.size.width/2;
    self.runnerAvatar.layer.masksToBounds = YES;
    
    self.runLastCommentAvatar.layer.cornerRadius = self.runLastCommentAvatar.frame.size.width/2;
    self.runLastCommentAvatar.layer.masksToBounds = YES;

    
    //Bordes
    [self addBorderWithColor:[UIColor grayColor] andWidth:5 atPosition:PREBorderPositionBottom];
    [self.runLastCommentContainer addBorderWithColor:[UIColor grayColor] andWidth:1 atPosition:PREBorderPositionBottom];
    
    [self.runLastCommentContainer addBorderWithColor:[UIColor grayColor] andWidth:1 atPosition:PREBorderPositionTop];
    
    [self.runPaceContainer addBorderWithColor:[UIColor grayColor] andWidth:1 atPosition:PREBorderPositionTop];
    [self.runDistanceContainer addBorderWithColor:[UIColor grayColor] andWidth:1 atPosition:PREBorderPositionTop];
    [self.runDurationContainer addBorderWithColor:[UIColor grayColor] andWidth:1 atPosition:PREBorderPositionTop];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:FALSE animated:NO];

}



+(NSString *)cellID{
    
    return NSStringFromClass(self);
}
+(CGFloat)height{
    
    return 615;
}

@end
