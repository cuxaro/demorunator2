// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BZZRunner.h instead.

#import <CoreData/CoreData.h>

extern const struct BZZRunnerAttributes {
	__unsafe_unretained NSString *foto;
	__unsafe_unretained NSString *identificador;
	__unsafe_unretained NSString *nombre;
} BZZRunnerAttributes;

extern const struct BZZRunnerRelationships {
	__unsafe_unretained NSString *commentrunner;
	__unsafe_unretained NSString *runrunner;
} BZZRunnerRelationships;

@class BZZComment;
@class BZZRun;

@interface BZZRunnerID : NSManagedObjectID {}
@end

@interface _BZZRunner : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BZZRunnerID* objectID;

@property (nonatomic, strong) NSData* foto;

//- (BOOL)validateFoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identificador;

//- (BOOL)validateIdentificador:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nombre;

//- (BOOL)validateNombre:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *commentrunner;

- (NSMutableSet*)commentrunnerSet;

@property (nonatomic, strong) NSSet *runrunner;

- (NSMutableSet*)runrunnerSet;

@end

@interface _BZZRunner (CommentrunnerCoreDataGeneratedAccessors)
- (void)addCommentrunner:(NSSet*)value_;
- (void)removeCommentrunner:(NSSet*)value_;
- (void)addCommentrunnerObject:(BZZComment*)value_;
- (void)removeCommentrunnerObject:(BZZComment*)value_;

@end

@interface _BZZRunner (RunrunnerCoreDataGeneratedAccessors)
- (void)addRunrunner:(NSSet*)value_;
- (void)removeRunrunner:(NSSet*)value_;
- (void)addRunrunnerObject:(BZZRun*)value_;
- (void)removeRunrunnerObject:(BZZRun*)value_;

@end

@interface _BZZRunner (CoreDataGeneratedPrimitiveAccessors)

- (NSData*)primitiveFoto;
- (void)setPrimitiveFoto:(NSData*)value;

- (NSString*)primitiveIdentificador;
- (void)setPrimitiveIdentificador:(NSString*)value;

- (NSString*)primitiveNombre;
- (void)setPrimitiveNombre:(NSString*)value;

- (NSMutableSet*)primitiveCommentrunner;
- (void)setPrimitiveCommentrunner:(NSMutableSet*)value;

- (NSMutableSet*)primitiveRunrunner;
- (void)setPrimitiveRunrunner:(NSMutableSet*)value;

@end
