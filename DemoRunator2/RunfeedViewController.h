//
//  ViewController.h
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;



@interface RunfeedViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)showNewRunTouch:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *showNewRuns;

@property (strong, nonatomic) IBOutlet UILabel *internetDown;
@end

