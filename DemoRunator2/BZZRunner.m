#import "BZZRunner.h"

#import <UIKit/UIKit.h>

@interface BZZRunner ()

// Private interface goes here.

@end

@implementation BZZRunner

+(instancetype)runnerWithDyctionary:(NSDictionary *)dicc withContext:(NSManagedObjectContext *)context{
    
    BZZRunner *runner;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[BZZRunner entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identificador == %@", [dicc objectForKey:@"id"]];
    NSError *error;
    NSMutableArray *response = [[context executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    if ([response count] > 0) {
        runner = (BZZRunner *)[response objectAtIndex:0];
        
    }else{
        runner = [NSEntityDescription insertNewObjectForEntityForName:[BZZRunner entityName]
                                                          inManagedObjectContext:context];
        runner.identificador = [dicc objectForKey:@"id"];
        runner.nombre = [dicc objectForKey:@"name"];

        UIImage *photo = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dicc objectForKey:@"photo_thumb"]]]];
        NSData *dataPhoto = UIImageJPEGRepresentation(photo, 0.0);
        
        runner.foto = dataPhoto;
        
    }

    return runner;

}


@end
