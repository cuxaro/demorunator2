// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BZZRun.m instead.

#import "_BZZRun.h"

const struct BZZRunAttributes BZZRunAttributes = {
	.comentarios = @"comentarios",
	.distancia = @"distancia",
	.duracion = @"duracion",
	.fecha = @"fecha",
	.foto = @"foto",
	.identificador = @"identificador",
	.likes = @"likes",
	.lugar = @"lugar",
	.mapa = @"mapa",
	.ritmo = @"ritmo",
};

const struct BZZRunRelationships BZZRunRelationships = {
	.comment = @"comment",
	.runner = @"runner",
};

@implementation BZZRunID
@end

@implementation _BZZRun

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Run" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Run";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Run" inManagedObjectContext:moc_];
}

- (BZZRunID*)objectID {
	return (BZZRunID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"comentariosValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"comentarios"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"distanciaValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"distancia"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"duracionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"duracion"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"likesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"likes"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic comentarios;

- (int16_t)comentariosValue {
	NSNumber *result = [self comentarios];
	return [result shortValue];
}

- (void)setComentariosValue:(int16_t)value_ {
	[self setComentarios:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveComentariosValue {
	NSNumber *result = [self primitiveComentarios];
	return [result shortValue];
}

- (void)setPrimitiveComentariosValue:(int16_t)value_ {
	[self setPrimitiveComentarios:[NSNumber numberWithShort:value_]];
}

@dynamic distancia;

- (float)distanciaValue {
	NSNumber *result = [self distancia];
	return [result floatValue];
}

- (void)setDistanciaValue:(float)value_ {
	[self setDistancia:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveDistanciaValue {
	NSNumber *result = [self primitiveDistancia];
	return [result floatValue];
}

- (void)setPrimitiveDistanciaValue:(float)value_ {
	[self setPrimitiveDistancia:[NSNumber numberWithFloat:value_]];
}

@dynamic duracion;

- (int16_t)duracionValue {
	NSNumber *result = [self duracion];
	return [result shortValue];
}

- (void)setDuracionValue:(int16_t)value_ {
	[self setDuracion:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDuracionValue {
	NSNumber *result = [self primitiveDuracion];
	return [result shortValue];
}

- (void)setPrimitiveDuracionValue:(int16_t)value_ {
	[self setPrimitiveDuracion:[NSNumber numberWithShort:value_]];
}

@dynamic fecha;

@dynamic foto;

@dynamic identificador;

@dynamic likes;

- (int16_t)likesValue {
	NSNumber *result = [self likes];
	return [result shortValue];
}

- (void)setLikesValue:(int16_t)value_ {
	[self setLikes:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLikesValue {
	NSNumber *result = [self primitiveLikes];
	return [result shortValue];
}

- (void)setPrimitiveLikesValue:(int16_t)value_ {
	[self setPrimitiveLikes:[NSNumber numberWithShort:value_]];
}

@dynamic lugar;

@dynamic mapa;

@dynamic ritmo;

@dynamic comment;

@dynamic runner;

@end

