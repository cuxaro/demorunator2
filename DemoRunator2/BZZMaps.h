//
//  BZZGoogleMaps.h
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 30/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;



@interface BZZMaps : NSObject


+(CLLocationCoordinate2D)decodePolylineWithString:(NSString *)polyline;


+(MKPolyline *)generatePolylineWith:(NSString *)polylineString;
@end
