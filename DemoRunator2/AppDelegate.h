//
//  AppDelegate.h
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

