// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BZZComment.h instead.

#import <CoreData/CoreData.h>

extern const struct BZZCommentAttributes {
	__unsafe_unretained NSString *comentario;
	__unsafe_unretained NSString *identificador;
} BZZCommentAttributes;

extern const struct BZZCommentRelationships {
	__unsafe_unretained NSString *runcomment;
	__unsafe_unretained NSString *runnercomment;
} BZZCommentRelationships;

@class BZZRun;
@class BZZRunner;

@interface BZZCommentID : NSManagedObjectID {}
@end

@interface _BZZComment : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BZZCommentID* objectID;

@property (nonatomic, strong) NSString* comentario;

//- (BOOL)validateComentario:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identificador;

//- (BOOL)validateIdentificador:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) BZZRun *runcomment;

//- (BOOL)validateRuncomment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) BZZRunner *runnercomment;

//- (BOOL)validateRunnercomment:(id*)value_ error:(NSError**)error_;

@end

@interface _BZZComment (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveComentario;
- (void)setPrimitiveComentario:(NSString*)value;

- (NSString*)primitiveIdentificador;
- (void)setPrimitiveIdentificador:(NSString*)value;

- (BZZRun*)primitiveRuncomment;
- (void)setPrimitiveRuncomment:(BZZRun*)value;

- (BZZRunner*)primitiveRunnercomment;
- (void)setPrimitiveRunnercomment:(BZZRunner*)value;

@end
