// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BZZRunner.m instead.

#import "_BZZRunner.h"

const struct BZZRunnerAttributes BZZRunnerAttributes = {
	.foto = @"foto",
	.identificador = @"identificador",
	.nombre = @"nombre",
};

const struct BZZRunnerRelationships BZZRunnerRelationships = {
	.commentrunner = @"commentrunner",
	.runrunner = @"runrunner",
};

@implementation BZZRunnerID
@end

@implementation _BZZRunner

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Runner" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Runner";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Runner" inManagedObjectContext:moc_];
}

- (BZZRunnerID*)objectID {
	return (BZZRunnerID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic foto;

@dynamic identificador;

@dynamic nombre;

@dynamic commentrunner;

- (NSMutableSet*)commentrunnerSet {
	[self willAccessValueForKey:@"commentrunner"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"commentrunner"];

	[self didAccessValueForKey:@"commentrunner"];
	return result;
}

@dynamic runrunner;

- (NSMutableSet*)runrunnerSet {
	[self willAccessValueForKey:@"runrunner"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"runrunner"];

	[self didAccessValueForKey:@"runrunner"];
	return result;
}

@end

