//
//  BZZRest.h
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking/AFNetworking.h>



@interface BZZRest : NSObject

+(instancetype) sharedSingleton;
-(AFHTTPSessionManager *)queryAPI;

+(void)sendInternetDownNotification;
+(void)sendInternetUpNotification;

@end
