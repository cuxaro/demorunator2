#import "BZZRun.h"
#import "BZZFunctionsRunator.h"

#import "BZZRunner.h"
#import "BZZComment.h"


#import <UIKit/UIKit.h>


@interface BZZRun ()

// Private interface goes here.

@end

@implementation BZZRun

+(instancetype)newRunWithDyctionary:(NSDictionary *)dicc withContext:(NSManagedObjectContext *)context{
    

    if ([dicc objectForKey:@"run_id"] != [NSNull null] ) {
        
        if (![BZZRun runExist:[dicc objectForKey:@"run_id"] withContext:context]) {
            
            BZZRun *run = [NSEntityDescription insertNewObjectForEntityForName:[BZZRun entityName]
                                                        inManagedObjectContext:context];

            run.identificador = [dicc objectForKey:@"run_id"];
            
            if ([[dicc objectForKey:@"run"] objectForKey:@"distance"] != [NSNull null]) {
                run.distancia = [[dicc objectForKey:@"run"] objectForKey:@"distance"];
                
            }
            
            if ([[dicc objectForKey:@"run"] objectForKey:@"duration"] != [NSNull null]) {
                run.duracion = [[dicc objectForKey:@"run"] objectForKey:@"duration"];
                
            }
            
            if ([[dicc objectForKey:@"run"] objectForKey:@"city"] != [NSNull null]) {
                run.lugar = [[dicc objectForKey:@"run"] objectForKey:@"city"];
                
            }
            
            if ([[dicc objectForKey:@"likes"] objectForKey:@"count"] != [NSNull null]) {
                run.likes = [[dicc objectForKey:@"likes"] objectForKey:@"count"];
                
            }
            
            if ([[dicc objectForKey:@"run"] objectForKey:@"pace"] != [NSNull null]) {
                run.ritmo = [[dicc objectForKey:@"run"] objectForKey:@"pace"];
                
            }
            
            if ([[dicc objectForKey:@"date"] objectForKey:@"date"] != [NSNull null]) {
                run.fecha = [BZZFunctionsRunator convertToDateFromString:[[dicc objectForKey:@"date"] objectForKey:@"date"]];
                
            }
            
            if ([dicc objectForKey:@"runator_user"] != [NSNull null]) {
                run.runner =  [BZZRunner runnerWithDyctionary:[dicc objectForKey:@"runator_user"] withContext:context];
                
            }
            
            if ([[dicc objectForKey:@"run"] objectForKey:@"photo"] != [NSNull null]) {
                
                UIImage *photo = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[dicc objectForKey:@"run"] objectForKey:@"photo"]]]];
                
                NSData *dataPhoto = UIImageJPEGRepresentation(photo, 0.0);
                run.foto = dataPhoto;

            }
            
            if ([[dicc objectForKey:@"comment_group"] objectForKey:@"total_hits"] != [NSNull null]) {
                run.comentarios = [[dicc objectForKey:@"comment_group"] objectForKey:@"total_hits"];
            }
            
            
            if ([[dicc objectForKey:@"comment_group"] objectForKey:@"last_comment"] != [NSNull null]) {
                run.comment = [BZZComment commentWithDyctionary:[[dicc objectForKey:@"comment_group"] objectForKey:@"last_comment"] withContext:context];
            }
            
            if ([[dicc objectForKey:@"run"] objectForKey:@"polyline"]) {
                run.mapa = [[dicc objectForKey:@"run"] objectForKey:@"polyline"];
                
            }
            
            [BZZRun removeRunsAfter:50 withContext:context];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newRuns" object:nil];
            
            return run;
        }
    }
    return nil;
    
    
}


+(BOOL)runExist:(NSString *)identifier withContext:(NSManagedObjectContext *)context{
        
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[BZZRun entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identificador == %@", identifier];
    
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"fecha"
                                                                   ascending:NO]];
    NSError *error;
    NSMutableArray *response = [[context executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    return [response count];

}

+(void)removeRunsAfter:(NSInteger)maxRuns withContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[BZZRun entityName]];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"fecha"
                                                                ascending:NO]];
    
    NSError *error;
    NSArray *response = [context executeFetchRequest:fetchRequest error:&error];
    
    NSInteger objetos = [response count] - maxRuns;
    
    if (objetos > 0) {
        for (NSInteger ciclos = 0; ciclos<objetos; ciclos++) {
            [context deleteObject:(NSManagedObject *)[response lastObject]];
        }
    }

}

@end
