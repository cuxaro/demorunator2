//
//  RunfeedCell.h
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 29/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;

@interface RunfeedCell : UITableViewCell


//Runner Info

@property (strong, nonatomic) IBOutlet UIImageView *runnerAvatar;
@property (strong, nonatomic) IBOutlet UILabel *runnerName;


//Run Data
@property (strong, nonatomic) IBOutlet UILabel *runPlace;
@property (strong, nonatomic) IBOutlet UILabel *runDate;
@property (strong, nonatomic) IBOutlet UILabel *runTime;

@property (strong, nonatomic) IBOutlet UIView *runMapContainer;
@property (strong, nonatomic) IBOutlet UIImageView *runPhoto;
@property (strong, nonatomic) IBOutlet MKMapView *runMapMapa;


@property (strong, nonatomic) IBOutlet UIView *runDistanceContainer;
@property (strong, nonatomic) IBOutlet UILabel *runDistanceText;

@property (strong, nonatomic) IBOutlet UIView *runPaceContainer;
@property (strong, nonatomic) IBOutlet UILabel *runPaceText;

@property (strong, nonatomic) IBOutlet UIView *runDurationContainer;
@property (strong, nonatomic) IBOutlet UILabel *runDurationText;


//Run Social
@property (strong, nonatomic) IBOutlet UILabel *runLikes;
@property (strong, nonatomic) IBOutlet UIView *runLastCommentContainer;

@property (strong, nonatomic) IBOutlet UIImageView *runLastCommentAvatar;
@property (strong, nonatomic) IBOutlet UILabel *runLastCommentName;
@property (strong, nonatomic) IBOutlet UILabel *runLastCommentText;

@property (strong, nonatomic) IBOutlet UILabel *runTotalComments;



//Class Methos
+(NSString *)cellID;
+(CGFloat)height;


@end
