// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BZZComment.m instead.

#import "_BZZComment.h"

const struct BZZCommentAttributes BZZCommentAttributes = {
	.comentario = @"comentario",
	.identificador = @"identificador",
};

const struct BZZCommentRelationships BZZCommentRelationships = {
	.runcomment = @"runcomment",
	.runnercomment = @"runnercomment",
};

@implementation BZZCommentID
@end

@implementation _BZZComment

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Comment" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Comment";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Comment" inManagedObjectContext:moc_];
}

- (BZZCommentID*)objectID {
	return (BZZCommentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic comentario;

@dynamic identificador;

@dynamic runcomment;

@dynamic runnercomment;

@end

