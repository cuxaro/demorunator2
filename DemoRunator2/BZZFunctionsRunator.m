//
//  BZZFunctionsRunator.m
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import "BZZFunctionsRunator.h"

@implementation BZZFunctionsRunator

+(NSDecimalNumber *)convertToNumberPace:(NSDictionary *)pace{
    
    NSInteger hour = [[pace objectForKey:@"hours"] integerValue] * 3600;
    NSInteger min = [[pace objectForKey:@"minute"] integerValue] * 60;
    NSInteger sec = [[pace objectForKey:@"second"] integerValue];
    
    NSNumber *num = [NSNumber numberWithInteger:hour+min+sec];
    NSDecimalNumber *dec = [NSDecimalNumber decimalNumberWithDecimal:[num decimalValue]];
    
    return dec;
    
}


+(NSDate *)convertToDateFromString:(NSString *)date{

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [dateFormat dateFromString:date];
    
}

+(NSDateFormatter *)formatDateWithString:(NSString *)stringFormat{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:stringFormat];
    [dateFormat setLocale:[NSLocale currentLocale]];
    
    return dateFormat;
    
}

+(NSString *)convertStandartDurationFormatFromSecond:(NSUInteger)seconds{
    
    NSUInteger h = seconds / 3600;
    NSUInteger m = (seconds / 60) % 60;
    NSUInteger s = seconds % 60;
    
    NSString *formattedTime = [NSString stringWithFormat:@"%luh %02lum %02lus", (unsigned long)h, (unsigned long)m, (unsigned long)s];
    
    return formattedTime;
    
}



@end
