//
//  BZZFunctionsRunator.h
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 28/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BZZFunctionsRunator : NSObject

+(NSDecimalNumber *)convertToNumberPace:(NSDictionary *)pace;
+(NSDate *)convertToDateFromString:(NSString *)date;

+(NSDateFormatter *)formatDateWithString:(NSString *)stringFormat;

+(NSString *)convertStandartDurationFormatFromSecond:(NSUInteger)seconds;

@end
