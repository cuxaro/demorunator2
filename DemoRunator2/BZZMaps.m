//
//  BZZGoogleMaps.m
//  DemoRunator2
//
//  Created by Ivan Cuxaro on 30/3/16.
//  Copyright © 2016 BuzzAPI. All rights reserved.
//

#import "BZZMaps.h"
@import CoreLocation;
@import MapKit;

@implementation BZZMaps
/*
+(GMSMapView *)googleMapWithPolyne:(NSString *)polyline{
    
    
    CLLocationCoordinate2D polylineDecoded = [BZZGoogleMaps decodePolylineWithString:polyline];
    
    NSLog(@"%@", [BZZGoogleMaps decodePolyLine:polyline]);
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:polylineDecoded.latitude
                                                            longitude:polylineDecoded.longitude
                                                                 zoom:6];
    
    GMSMapView *map = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    map.myLocationEnabled = YES;
    
    return map;
}
 */

+(CLLocationCoordinate2D)decodePolylineWithString:(NSString *)polyline{
    

    const char *bytes = [polyline UTF8String];
    //NSUInteger length = [polyline lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    
    float latitude = 0;
    float longitude = 0;
    //CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(latitude, longitude);

        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);

        //coords[coordIdx++] = coord;
        
        /*
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
         */
    


    //free(coords);
    
    return coord;
}

+(MKPolyline *)generatePolylineWith:(NSString *)polylineString {
    
    
    
    // create a polyline with all cooridnates
    NSMutableString *encoded = [polylineString mutableCopy];
    
    
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
        NSLog(@"%f,%f",[latitude floatValue],[longitude floatValue]);

    }

    
    CLLocationCoordinate2D *coordinateArray = malloc(sizeof(CLLocationCoordinate2D) * array.count);
    
    int caIndex = 0;
    for (CLLocation *loc in array) {
        coordinateArray[caIndex] = loc.coordinate;
        caIndex++;
    }
    
    MKPolyline *lines = [MKPolyline polylineWithCoordinates:coordinateArray
                                                      count:array.count];
    
    free(coordinateArray);
    return lines;
    
}


@end
