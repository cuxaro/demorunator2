#import "_BZZRunner.h"

@interface BZZRunner : _BZZRunner {}


+(instancetype)runnerWithDyctionary:(NSDictionary *)dicc withContext:(NSManagedObjectContext *)context;


@end
