// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BZZRun.h instead.

#import <CoreData/CoreData.h>

extern const struct BZZRunAttributes {
	__unsafe_unretained NSString *comentarios;
	__unsafe_unretained NSString *distancia;
	__unsafe_unretained NSString *duracion;
	__unsafe_unretained NSString *fecha;
	__unsafe_unretained NSString *foto;
	__unsafe_unretained NSString *identificador;
	__unsafe_unretained NSString *likes;
	__unsafe_unretained NSString *lugar;
	__unsafe_unretained NSString *mapa;
	__unsafe_unretained NSString *ritmo;
} BZZRunAttributes;

extern const struct BZZRunRelationships {
	__unsafe_unretained NSString *comment;
	__unsafe_unretained NSString *runner;
} BZZRunRelationships;

@class BZZComment;
@class BZZRunner;

@interface BZZRunID : NSManagedObjectID {}
@end

@interface _BZZRun : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BZZRunID* objectID;

@property (nonatomic, strong) NSNumber* comentarios;

@property (atomic) int16_t comentariosValue;
- (int16_t)comentariosValue;
- (void)setComentariosValue:(int16_t)value_;

//- (BOOL)validateComentarios:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* distancia;

@property (atomic) float distanciaValue;
- (float)distanciaValue;
- (void)setDistanciaValue:(float)value_;

//- (BOOL)validateDistancia:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* duracion;

@property (atomic) int16_t duracionValue;
- (int16_t)duracionValue;
- (void)setDuracionValue:(int16_t)value_;

//- (BOOL)validateDuracion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* fecha;

//- (BOOL)validateFecha:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* foto;

//- (BOOL)validateFoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identificador;

//- (BOOL)validateIdentificador:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* likes;

@property (atomic) int16_t likesValue;
- (int16_t)likesValue;
- (void)setLikesValue:(int16_t)value_;

//- (BOOL)validateLikes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* lugar;

//- (BOOL)validateLugar:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* mapa;

//- (BOOL)validateMapa:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* ritmo;

//- (BOOL)validateRitmo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) BZZComment *comment;

//- (BOOL)validateComment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) BZZRunner *runner;

//- (BOOL)validateRunner:(id*)value_ error:(NSError**)error_;

@end

@interface _BZZRun (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveComentarios;
- (void)setPrimitiveComentarios:(NSNumber*)value;

- (int16_t)primitiveComentariosValue;
- (void)setPrimitiveComentariosValue:(int16_t)value_;

- (NSNumber*)primitiveDistancia;
- (void)setPrimitiveDistancia:(NSNumber*)value;

- (float)primitiveDistanciaValue;
- (void)setPrimitiveDistanciaValue:(float)value_;

- (NSNumber*)primitiveDuracion;
- (void)setPrimitiveDuracion:(NSNumber*)value;

- (int16_t)primitiveDuracionValue;
- (void)setPrimitiveDuracionValue:(int16_t)value_;

- (NSDate*)primitiveFecha;
- (void)setPrimitiveFecha:(NSDate*)value;

- (NSData*)primitiveFoto;
- (void)setPrimitiveFoto:(NSData*)value;

- (NSString*)primitiveIdentificador;
- (void)setPrimitiveIdentificador:(NSString*)value;

- (NSNumber*)primitiveLikes;
- (void)setPrimitiveLikes:(NSNumber*)value;

- (int16_t)primitiveLikesValue;
- (void)setPrimitiveLikesValue:(int16_t)value_;

- (NSString*)primitiveLugar;
- (void)setPrimitiveLugar:(NSString*)value;

- (NSString*)primitiveMapa;
- (void)setPrimitiveMapa:(NSString*)value;

- (NSDecimalNumber*)primitiveRitmo;
- (void)setPrimitiveRitmo:(NSDecimalNumber*)value;

- (BZZComment*)primitiveComment;
- (void)setPrimitiveComment:(BZZComment*)value;

- (BZZRunner*)primitiveRunner;
- (void)setPrimitiveRunner:(BZZRunner*)value;

@end
