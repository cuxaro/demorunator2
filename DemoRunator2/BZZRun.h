#import "_BZZRun.h"

@interface BZZRun : _BZZRun {}
// Custom logic goes here.


+(instancetype)newRunWithDyctionary:(NSDictionary *)dicc withContext:(NSManagedObjectContext *)context;
+(BOOL)runExist:(NSString *)identifier withContext:(NSManagedObjectContext *)context;

+(void)removeRunsAfter:(NSInteger)maxRuns withContext:(NSManagedObjectContext *)context;
@end
